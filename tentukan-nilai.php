<?php
function tentukan_nilai($number)
{
    //  kode disini
    if ($number<=100 && $number>=85){
        echo $number . " : Sangat Baik <br>";
    }else if($number <85 && $number>=70){
        echo $number . " : Baik <br>";
    }else if($number <70 && $number>=60){
        echo $number . " : Cukup <br>";
    }else{
        echo $number . " : Kurang";
    }
        
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
?>