<?php
function ubah_huruf($string)
{
    //kode di sini
    $cari  = ["wow", "developer", "laravel", "keren", "semangat"];
    $ganti = ["xpx", "efwfmpqfs", "mbsbwfm", "lfsfo", "tfnbohbu"];
    echo str_replace($cari, $ganti, $string);
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo "<br>";
echo ubah_huruf('developer'); // efwfmpqfs
echo "<br>";
echo ubah_huruf('laravel'); // mbsbwfm
echo "<br>";
echo ubah_huruf('keren'); // lfsfo
echo "<br>";
echo ubah_huruf('semangat'); // tfnbohbu

?>